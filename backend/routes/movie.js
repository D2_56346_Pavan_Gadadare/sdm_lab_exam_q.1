const express = require("express");
const router = express.Router();
const utils = require("../utils");
const db = require("../db");
const { request } = require("http");
const { response } = require("express");

//1. GET  --> Display Movie using name from Containerized MySQL

router.get("/:movie_title" , (request,response) => {

    const {movie_title} = request.params;
    const connection = db.openConnection();

    const statement = ` select * from Movie where 
                        movie_title = '${movie_title}'  `

    connection.query(statement , (error,result) => {
        connection.end();
        if(result.length>0){
            console.log(result);
            response.send(utils.createResult(error,result));
        }
        else{
            response.send("no movie found");
        }
    });                    
});


//2. POST --> ADD Movie data into Containerized MySQL table

router.post("/add" , (request,response) => {

    const {movie_id,movie_title,movie_release_date,
        movie_time,director_name} = request.body;

    const connection = db.openConnection();

    const statement = ` insert into movie (movie_id,
        movie_title,
        movie_release_date,
        movie_time,
        director_name) values ( '${movie_id}', '${movie_title}' ,
            '${movie_release_date}' ,'${movie_time}' ,
            '${director_name}' ) `;

    connection.query(statement , (error,result) => {
        connection.end();
            console.log(result);
            response.send(utils.createResult(error,result));
    });                    
});

//3. UPDATE --> Update Release_Date and Movie_Time into Containerized MySQL table


router.put("/update/:movie_title" , (request,response) => {

    const {movie_title} = request.params;
    const {movie_release_date,
        movie_time} = request.body;

    const connection = db.openConnection();

    const statement = ` update Movie set 
    movie_release_date = '${movie_release_date}' AND
    movie_time = '${movie_time}'
    where movie_title = '${movie_title}' `;

    connection.query(statement , (error,result) => {
        connection.end();
            console.log(result);
            response.send(utils.createResult(error,result));
    });                    
});


//4. DELETE --> Delete Movie from Containerized MySQL
router.delete("/delete/:movie_title" , (request,response) => {

    const {movie_title} = request.params;
    const connection = db.openConnection();

    const statement = ` delete from movie 
    where movie_title = '${movie_title}' `;

    connection.query(statement , (error,result) => {
        connection.end();
            console.log(result);
            response.send(utils.createResult(error,result));
    });                    
});



module.exports = {router};















