const express = require("express");
const app = express();
const movieRoutes = require("./routes/movie");
const cors = require("cors");

app.use(cors("*"));
app.use(express.json());
app.use("/",movieRoutes);

app.listen(4000 , "0.0.0.0" , () => {
    console.log("server started on port 4000");
});