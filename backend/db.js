const mysql = require("mysql");

const openConnection = () => {

    const connection = mysql.createConnection({
        uri:"mysql://db:3306",
        user:"root",
        password: "root",
        database:"sdm_lab_exam"
    });
    connection.connect();
    return connection;
};

module.exports = {openConnection};